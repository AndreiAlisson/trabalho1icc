#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "utils.h"
#include "ZeroFuncao.h"

double df(const double x){

    return(1);//q

}

double f(const double x){

	return((x*x) - (5*x) + 4);

}

int main ()
{

	double x = 2.0, raiz, eps = 0.001, a = 0.0, b = 3.0;
	int iter = 0, error;


/***********************BICECCAO*********************/

	error = bisseccao(f, a, b, eps, &iter, &raiz);

	if(error == 0)
        printf("biceccao resposta encontrada: x = %f\n", raiz);
    else if(error == 1)
        printf("Erro: Ultrapassou o numero maximo de iteracoes\n");
    else if(error == 2)
        printf("erro fa*fb>0\n");
    else if(error == 3)
        printf("Funcao nao convergiu\n");

/**********************FIM BICECCAO***********************************/

/*************************NEWTON******************************/

    iter = 0;
    raiz = 0;

    error = newton(f, df, 6.0, eps, &iter, &raiz);

	if(error == 0)
        printf("newton resposta encontrada: x = %f\n", raiz);

/*************************FIM NEWTON******************************/

/*************************SECANTE******************************/
    iter = 0;
    raiz = 0;

    error = secante(f, 5.0, 6.0, eps, &iter, &raiz);

	if(error == 0)
        printf("secante resposta encontrada: x = %f\n", raiz);

/****************************FIM SECANTE*******************************************/

    Polinomio p;
    p.p = malloc(3*sizeof(double));
    p.p[2] = 1;
    p.p[1] = -5;
    p.p[0] = 4;
    p.grau = 2;

    double px, dpx;

    calcPolinomioEDerivada(p, x, &px, &dpx);

    printf("Polinomio e derivada no ponto %f: %f e %f\n", x, px, dpx);


    srand((unsigned int)time(NULL));

    unsigned long n = 5;
    double v[n];

    for(unsigned long i = 0; i < n; i++)
        v[i] = (float)rand()/(float)(RAND_MAX/10000);

    double v2[n];
    v2[0] = 0.00005;
    v2[1] = 20;
    v2[2] = 30;
    v2[3] = 40;
    v2[4] = 500000;

    printf("Media calculada: %f\n", media(v, n));



  return 0;
}

