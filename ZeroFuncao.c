/**
 *Nome: Andrei Alisson Ferreira Julio GRR20163061*
 */

#include <stdio.h>
#include <math.h>

#include "utils.h"
#include "ZeroFuncao.h"

//! Função Biseccao
/*!
 \param *f(x) é o ponteiro pra função definida no arquivo fonte principal.
 \param a é o valor inicial estimado no intervalo de convergência
 \param é o valor final estimado no intervalo de convergência
 \param eps é o erro definido
 \param *it é o número de iterações realizadas pela função
 \param *raiz é o ponteiro pra variável à ser armazenado o valor da raíz da equação (i.e. x | f(x) = 0)
 \return 0 em caso de sucesso, 1 caso o limite de iterações seja ultrapassado sem encontrar a resposta, 2 caso a função não converja (i.e. f(a)*f(b) > 0).
*/
int bisseccao (double (*f)(const double x), double a, double b,
               double eps, int *it, double *raiz)
{

	double fa = f(a), fb = f(b);

	if(fa*fb < 0.0){ /**>Se os f(a)*f(b) for menor que zero a função convergirá em casos normais*/
		double x_act = b, x_old = a, fx;

		do{

            fa = f(a);
            fb = f(b);
            x_old = x_act;
			x_act = (a+b) / 2.0;
			fx = f(x_act);

            if(fx == 0.0) /**>Se f(x) no ponto é zero encontrou a raíz, logo sai do laço e retorna 0*/
                break;
			else if(fabs((x_act-x_old))/fabs(x_act) < eps) /**>Senão, se o erro relativo calculado é menor que eps, aceita como raíz, saindo do laço e retornando 0*/
                break;
            else if(fa * fx < 0.0)
				b = x_act;
			else if(fb * fx < 0.0)
				a = x_act;
            else{ /**>Se não cai neste else significa que por algum motivo a função parou de convergir e é retornado 3*/
                fprintf(stderr, "Erro na funcao biseccao: Funcao não convergiu\n");
                return 3;
            }

		}while(++(*it) < MAXIT);


		if(*it < MAXIT){ /**>Se saiu do laço sem ultrapassar a quantidade máxima de iterações permitidas, a raíz foi encontrada e é retornado 0*/
			*raiz = x_act;
			return 0;
		}
		else{ /**>Se saiu do laço e ultrapassou a quantidade máxima de iterações permitidas, a raíz não foi encontrada e é retornado 1*/
            fprintf( stderr, "Erro na funcao biseccao: Ultrapassou o numero maximo de iteracoes\n");
            return 1;
		}

	}
	else{ /**>Se os valores da função nos pontos a e b possuem o mesmo sinal, a função é encerrada e é retornado 2*/
        fprintf( stderr, "Erro na funcao biseccao: Valores de f(a) e f(b) possuem o mesmo sinal\n");
		return 2;
	}

	return 0;
}

//! Função newton
/*!
 \param *f(x) é o ponteiro pra função definida no arquivo fonte que contém o main.
 \param *df(x) é o ponteiro pra função que calcula derivada definida no arquivo que contém o main.
 \param x0 é o valor inicial de xi
 \param eps é o erro definido
 \param *it é o número de iterações realizadas pela função
 \param *raiz é o ponteiro pra variável à ser armazenado o valor da raíz da equação (i.e. x | f(x) = 0)
 \return 0 em caso de sucesso, 1 caso o limite de iterações seja ultrapassado sem encontrar a resposta, 2 caso haja alguma divisão por 0.
*/
int newton (double (*f)(const double x), double (*df)(const double x), double x0,
            double eps, int *it, double *raiz)
{

    double newton = x0, old_newton, fx, dfx;

    do{
        old_newton = newton;
        dfx = df(old_newton);
        newton = old_newton - (f(old_newton)/dfx);
        if(isinf(newton)){ /**>Verifica se houve divisão por zero, caso sim, retorna 2*/
            fprintf(stderr, "Erro na funcao newton: Divisao por zero\n");
            return 2;
        }

        fx = f(newton);

        if(fx == 0.0)
            break;
        else if(fabs(((newton - old_newton)/newton)*100) < eps);
            break;

    }while(*it < MAXIT);

    if(*it < MAXIT){ /**>Se saiu do laço sem ultrapassar a quantidade máxima de iterações permitidas, a raíz foi encontrada e é retornado 0*/
        *raiz = newton;
        return 0;
    }
    else{ /**>Se saiu do laço e ultrapassou a quantidade máxima de iterações permitidas, a raíz não foi encontrada e é retornado 1*/
        fprintf( stderr, "Erro na funcao newton: Ultrapassou o numero maximo de iteracoes\n");
        return 1;
    }

    return 0;
}

//! Função secante
/*!
 \param *f(x) é o ponteiro pra função definida no arquivo fonte que contém o main.
 \param x0 é o primeiro valor inicial de xi
 \param x1 é o segundo valor inicial de xi
 \param eps é o erro definido
 \param *it é o número de iterações realizadas pela função
 \param *raiz é o ponteiro pra variável à ser armazenado o valor da raíz da equação (i.e. x | f(x) = 0)
 \return 0 em caso de sucesso, 1 caso o limite de iterações seja ultrapassado sem encontrar a resposta, 2 caso haja alguma divisão por zero.
*/
int secante (double (*f)(const double x), double x0, double x1,
             double eps, int *it, double *raiz)
{

    double fx, x2;

    do{
        x2 = x1 - ((f(x1) * (x1-x0))/(f(x1) - f(x0)));
        if(isinf(x2)){ /**>Verifica se houve divisão por zero, caso sim, retorna 2*/
            fprintf(stderr, "Erro na funcao secante: Divisao por zero\n");
            return 2;
        }
        fx = f(x2);

        /*printf("f(x) na it %d: %f\n", *it, fx); //VERIFICAR SE HOUVE DIVISÃO POR ZERO
        printf("  %f < %f\n", fabs(((x2 - x1)/x2)*100), eps);
        printf("x2 = %f x1 = %f\n", x2, x1);*/

        if(fx == 0.0)
            break;
        else if(fabs(((x2 - x1)/x2)*100) < eps)
            break;

        x0 = x1;
        x1 = x2;

    }while(++(*it) < MAXIT);

    if(*it < MAXIT){
        *raiz = x2;
        return 0;
    }
    else{ /**>Se saiu do laço e ultrapassou a quantidade máxima de iterações permitidas, a raíz não foi encontrada e é retornado 1*/
        fprintf( stderr, "Erro na funcao secante: Ultrapassou o numero maximo de iteracoes\n");
        return 1;
    }

    return 0;
}

//! Função Calcula Polinômio.
/*! Calcula polinômio no ponto x e sua derivada, onde o formato da estrutura p deve ser: p.p[0] = a0, p.p[1] = a1*x, p.p[2] = a2*x^2... (Os elementos do vetor são os coeficientes e o grau é o grau do polinômio)
 \param p é o polinômio
 \param x é o valor de x a ser calculado no polinômio e sua derivada
 \param px é a variável que vai receber a resposta do cálculo do polinômio no ponto x
 \param dpx é a variável que vai receber a resposta do cálculo da derivada do polinômio no ponto x
 \return 1 caso o polinômio seja nulo, 0 caso contrário (i.e. caso o polinômio possa ser calculado e derivado)
*/
int calcPolinomioEDerivada(Polinomio p, double x, double *px, double *dpx )
{

    if(p.grau < 1)
        return 1;

    int i;

	*px = *dpx = 0;

	*px += p.p[0] * pow(x, 0);
	for(i = 1; i <= p.grau; i++){
		*px += p.p[i] * pow(x, i);
		*dpx += p.p[i] * i * pow(x, i-1);
	}

    return 0;
}

//! Função media
/*! Calcula a média de um vetor de ponto flutuante calculando médias parciais de dois em dois elementos.
 \param valores é o vetor
 \param n é o tamanho do vetor
 \return a média calculada.
 */
double media(double *valores, unsigned long n)
{
    unsigned long it;
    it = floor(n/2);
    double media = 0;

    for(unsigned long i = 0; i < n - (n % 2); i+= it)
        media += ((valores[i] + valores[i+1])/n);

    if(n % 2 == 1)
        media += (valores[n-1]/n);

    return media;
}


